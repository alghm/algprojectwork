﻿using Microsoft.CodeAnalysis.Scripting;
using BenchmarkDotNet.Attributes;
using Microsoft.CodeAnalysis.CSharp.Scripting;

namespace AlgorithmsEvaluationPerformanceApp
{
    /*
    public class BenchmarkClass
    {
        private static string _userCode;

        public static void SetUserCode(string userCode)
        {
            _userCode = userCode;
        }

        [Benchmark]
        public async Task TestUserCode()
        {
            var scriptOptions = ScriptOptions.Default.WithReferences(typeof(object).Assembly);
            var script = CSharpScript.Create(_userCode, scriptOptions);
            await script.RunAsync();
        }
    }
    */

    public class CustomBenchmark
    {
        private static string _userCode;

        public static void SetUserCode(string userCode)
        {
            _userCode = userCode;
        }

        [Benchmark]
        public async Task TestUserCode()
        {
            var scriptOptions = ScriptOptions.Default.WithReferences(typeof(object).Assembly);
            var script = CSharpScript.Create(_userCode, scriptOptions);
            await script.RunAsync();
        }
    }
}
