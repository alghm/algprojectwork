﻿namespace AlgorithmsEvaluationPerformanceApp.Models
{
    public class BenchmarkModel
    {
        public string UserCode { get; set; }
        public int[] BenchmarkSizeParams { get; set; }
    }
}
