﻿using AlgorithmsEvaluationPerformanceApp.Models;
using AlgorithmsEvaluationPerformanceApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AlgorithmsEvaluationPerformanceApp.Controllers
{
    public class UserCodeController : Controller
    {
        List<int> PossibleSizes = new() { 10, 100, 1000, 10_000, 100_000, 1_000_000, 10_000_000 };

        public UserCodeController()
        {
            
        }


        [HttpGet]
        public IActionResult Run()
        {
            var model = new BenchmarkModel();

            var selectListItems = PossibleSizes.Select(pv => new SelectListItem
            {
                Value = pv.ToString(),
                Text = pv.ToString()
            }).ToList();

            model.BenchmarkSizeParams = PossibleSizes.ToArray();

            ViewBag.PossibleValues = selectListItems;

            return View(model);
        }

        [HttpPost]
        public IActionResult Run(BenchmarkModel model)
        {
            ViewData["UserInput"] = model.UserCode;

            var finalCode = CodeTemplateService.GetBenchmarkCode(model.UserCode, model.BenchmarkSizeParams);
            FileService.SaveConsoleProjectBenchmarkFile(finalCode);
            ApplicationService.RunDotNetConsoleProject();

            string reportPath;

            while (true)
            {
                reportPath = BenchmarkReportService.GetReportPath();
                if (reportPath != string.Empty)
                {
                    break;
                }
                Task.Delay(3000); // Ждем 3 секунду перед повторной проверкой
            }

            return PhysicalFile(reportPath, "text/html");
        }
    }
}