﻿namespace AlgorithmsEvaluationPerformanceApp.Services
{
    public static class CodeTemplateService
    {
        const string codeTemplate = "using BenchmarkDotNet.Attributes;\r\n\r\nnamespace BenchmarkConsole\r\n{\r\n    public class CustomBenchmark\r\n    {\r\n        private int[] _sourceArray;\r\n\r\n        [Params()]\r\n        public int Size { get; set; }\r\n\r\n        [GlobalSetup]\r\n        public void Setup()\r\n        {\r\n            _sourceArray = Enumerable.Range(0, Size).ToArray();\r\n        }\r\n\r\n        [Benchmark]\r\n        public void Method() {}\r\n    }\r\n}\r\n";
        const string oldMethodContent = "public void Method() {}";
        const string oldParams = "Params()";

        public static string GetBenchmarkCode(string newMethodContent, int[] sizeParams)
        {
            return codeTemplate.Replace(oldMethodContent, newMethodContent).Replace(oldParams, $"Params({ string.Join(", ", sizeParams)})");
        }
    }
}
