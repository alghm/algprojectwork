﻿using System.Diagnostics;

namespace AlgorithmsEvaluationPerformanceApp.Services
{
    public static class ApplicationService
    {
        public static bool RunDotNetConsoleProject()
        {
            try
            {
                var projectPath = FileService.GetConsoleBenchmarkProjectPath();

                // Build the project
                Process process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = $"/C cd /d {projectPath} && dotnet build -c Release";
                process.StartInfo.RedirectStandardOutput = false;
                process.StartInfo.UseShellExecute = false;
                process.Start();
                process.WaitForExit();

                // Run the project
                process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = $"/C cd /d {projectPath} && dotnet run -c Release";
                process.StartInfo.RedirectStandardOutput = false;
                process.StartInfo.UseShellExecute = false;
                process.Start();
                process.WaitForExit();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Произошла ошибка при запуске проекта - " + ex.Message);
                return false;
            }
        }
    }
}
