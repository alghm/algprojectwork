﻿using System.IO;

namespace AlgorithmsEvaluationPerformanceApp.Services
{
    public static class FileService
    {
        const string fileName = "CustomBenchmark.cs";
        const string targetFolder = "AlgorithmsEvaluationPerformanceApp";
        const string projectName = "BenchmarkConsole";
        static string basePath = AppContext.BaseDirectory;

        public static string GetConsoleBenchmarkProjectPath()
        {
            string slnPath;
            string projectPath = "";

            int index = basePath.LastIndexOf(targetFolder);

            if (index != -1)
            {
                int startIndex = basePath.Substring(0, index).LastIndexOf('\\') + 1;
                slnPath = basePath.Substring(0, startIndex);
                projectPath = Path.Combine(slnPath, projectName);
            }
            else
            {
                Console.WriteLine($"Папка проекта {targetFolder} не найдена");
            }

            return projectPath;
        }

        public static string SaveConsoleProjectBenchmarkFile(string sourceCode)
        {
            try
            {
                var originalProjectPath = GetConsoleBenchmarkProjectPath();
                var path = Path.Combine(originalProjectPath, fileName);
                using (StreamWriter writer = File.CreateText(path))
                {
                    writer.Write(sourceCode);
                }

                return path;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Произошла ошибка при сохранении файла - " + ex.Message);
                return string.Empty;
            }
        }
    }
}
