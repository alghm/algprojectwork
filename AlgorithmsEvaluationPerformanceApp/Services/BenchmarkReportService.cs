﻿namespace AlgorithmsEvaluationPerformanceApp.Services
{
    public static class BenchmarkReportService
    {
        const string originalReportPath = "BenchmarkDotNet.Artifacts\\results\\BenchmarkConsole.CustomBenchmark-report.html";

        public static string GetReportPath()
        {
            var reportPath = Path.Combine(FileService.GetConsoleBenchmarkProjectPath(), originalReportPath);
            if (Path.Exists(reportPath))
            {
                return reportPath;
            }

            return string.Empty;
        }
    }
}
