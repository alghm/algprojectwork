﻿using BenchmarkDotNet.Attributes;

namespace BenchmarkConsole
{
    public class UserBenchmark
    {
        private int[] _sourceArray;

        [Params(10, 100, 1000, 10_000, 100_000, 1_000_000, 10_000_000)]
        public int Size { get; set; }

        [GlobalSetup]
        public void Setup()
        {
            _sourceArray = Enumerable.Range(0, Size).ToArray();
        }

        [Benchmark]
        public bool BinarySearch()
        {
            int target = Size / 2; // Установка целевого значения
            int left = 0;
            int right = _sourceArray.Length - 1;

            while (left <= right)
            {
                int middle = (left + right) / 2;

                if (_sourceArray[middle] == target)
                    return true;

                if (_sourceArray[middle] < target)
                    left = middle + 1;
                else
                    right = middle - 1;
            }

            return false;
        }
    }
}
