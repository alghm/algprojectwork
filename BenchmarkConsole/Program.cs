﻿using BenchmarkDotNet.Running;

namespace BenchmarkConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Запуск бенчмарка *****");
            var summary = BenchmarkRunner.Run<CustomBenchmark>();
            Console.WriteLine();
        }
    }
}


public static class BenchmarkModel
{
    //[Obsolete]
    //public static void RunBenchmarkFromString(string code)
    //{
    //    // Запускаем бенчмарк
    //    var summary = BenchmarkRunner.RunSource(code);
    //    foreach (var report in summary.Reports)
    //    {
    //        Console.WriteLine(value: $"{report.BenchmarkCase.DisplayInfo}: {report.ResultStatistics.Mean}");
    //    }
    //}
}

public class BenchmarkRunner<T>
{
    public void Run()
    {
        BenchmarkRunner.Run<T>();
    }
}