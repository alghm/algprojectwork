using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;

namespace BenchmarkConsole
{
    [RPlotExporter]
public class CustomBenchmark
    {
        private int[] _sourceArray;

        [Params(100, 1000)]
        public int Size { get; set; }

        [GlobalSetup]
        public void Setup()
        {
            _sourceArray = Enumerable.Range(0, Size).ToArray();
        }

        [Benchmark]
        public void BubbleSort2()
{
    for (int i = 0; i < _sourceArray.Length; i++)
    {
        bool swapped = false;
        for (int j = 0; j < _sourceArray.Length - i - 1; j++)
        {
            if (_sourceArray[j] > _sourceArray[j + 1])
            {
                int temp = _sourceArray[j];
                _sourceArray[j] = _sourceArray[j + 1];
                _sourceArray[j + 1] = temp;

                swapped = true;
            }
        }

        if (!swapped)
        {
            break;
        }
    }
}

    }
}
