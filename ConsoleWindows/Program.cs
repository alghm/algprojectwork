﻿using BenchmarkDotNet.Running;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using BenchmarkDotNet.Attributes;
using Microsoft.CodeAnalysis.Emit;
using System.Runtime.InteropServices;

namespace ConsoleWindows
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Ваш исходный код
            string code = @"
                using BenchmarkDotNet.Attributes;
                using System.Linq;

                namespace ConsoleWindows
                {
                   public class UserBenchmark
                   {
                       private int[] _sortedNumbers;

                       [Params(10, 100, 1000, 10000, 100000, 1000000)]
                       public int Size { get; set; }

                       [GlobalSetup]
                       public void Setup()
                       {
                           _sortedNumbers = Enumerable.Range(0, Size).ToArray();
                       }

                       [Benchmark]
                       public bool BinarySearch()
                       {
                           int target = Size / 2; // Установка целевого значения
                           int left = 0;
                           int right = _sortedNumbers.Length - 1;

                           while (left <= right)
                           {
                             int middle = (left + right) / 2;

                             if (_sortedNumbers[middle] == target)
                                return true;

                             if (_sortedNumbers[middle] < target)
                                left = middle + 1;
                             else
                                right = middle - 1;
                           }

                           return false;
                       }
                   }
                 }";

            Console.WriteLine($"Object location: {typeof(object).Assembly.Location}");
            Console.WriteLine($"Console location: {typeof(Console).Assembly.Location}");
            Console.WriteLine($"Enumerable location: {typeof(System.Linq.Enumerable).Assembly.Location}");
            Console.WriteLine($"BenchmarkAttribute location: {typeof(BenchmarkAttribute).Assembly.Location}");
            Console.WriteLine($"Runtime directory: {RuntimeEnvironment.GetRuntimeDirectory()}");

            // Компиляция кода
            var syntaxTree = CSharpSyntaxTree.ParseText(code);
            var references = new MetadataReference[]
            {
                //MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
                //MetadataReference.CreateFromFile(typeof(Console).Assembly.Location),
                MetadataReference.CreateFromFile(typeof(System.Linq.Enumerable).Assembly.Location),
                MetadataReference.CreateFromFile(typeof(BenchmarkAttribute).Assembly.Location),
                MetadataReference.CreateFromFile(Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), "netstandard.dll"))
            };

            var compilation = CSharpCompilation.Create("MyCompilation", new[] { syntaxTree }, references, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary).WithOptimizationLevel(OptimizationLevel.Release));

            using (var ms = new MemoryStream())
            {
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic => diagnostic.IsWarningAsError || diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (Diagnostic diagnostic in failures)
                    {
                        Console.Error.WriteLine("{0}: {1}", diagnostic.Id, diagnostic.GetMessage());
                    }
                }
                else
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    Assembly assembly = Assembly.Load(ms.ToArray());

                    // Затем запустите бенчмарк
                    var summary_res = BenchmarkRunner.Run(assembly.GetTypes().FirstOrDefault(t => t.Name == "UserBenchmark"));
                }
            }


            Console.WriteLine();


            // var sd = BenchmarkRunner.Run<UserBenchmark>();

            code = @"
               using BenchmarkDotNet.Attributes;
               using System.Linq;

               namespace ConsoleWindows
               {
                   public class UserBenchmark
                   {
                       private int[] _sortedNumbers;

                       [Params(10, 100, 1000, 10000, 100000, 1000000)]
                       public int Size { get; set; }

                       [GlobalSetup]
                       public void Setup()
                       {
                           _sortedNumbers = Enumerable.Range(0, Size).ToArray();
                       }

                       [Benchmark]
                       public bool BinarySearch()
                       {
                           int target = _sortedNumbers[_sortedNumbers.Length / 2];
                           int left = 0;
                           int right = _sortedNumbers.Length - 1;

                           while (left <= right)
                           {
                              int middle = (left + right) / 2;

                              if (_sortedNumbers[middle] == target)
                                  return true;

                              if (_sortedNumbers[middle] < target)
                                  left = middle + 1;
                              else
                                  right = middle - 1;
                           }

                           return false;
                       }
                   }
                }";

            var filePath = @"C:\Users\Vladislav\source\repos\AlgProjectWork\ConsoleWindows\UserBenchmark.cs";

            File.WriteAllText(filePath, code);
            var summary = BenchmarkRunner.Run<UserBenchmark>();
            foreach (var report in summary.Reports)
            {
                Console.WriteLine($"Custom - {report.BenchmarkCase.DisplayInfo}: {report.ResultStatistics.Mean}");
            }

            Console.WriteLine();
        }
    }
}

public static class BenchmarkModel
{
    public static void RunBenchmarkFromString(string code)
    {
        // Запускаем бенчмарк
        var summary = BenchmarkRunner.RunSource(code);
        foreach (var report in summary.Reports)
        {
            Console.WriteLine($"Custom - {report.BenchmarkCase.DisplayInfo}: {report.ResultStatistics.Mean}");
        }
    }
}

public static class RuntimeCompilation
{
    public static Assembly CompileCode(string code)
    {
        var syntaxTree = CSharpSyntaxTree.ParseText(code);
        var references = GetReferences();

        var compilation = CSharpCompilation.Create("DynamicAssembly")
            .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
            .AddReferences(references)
            .AddSyntaxTrees(syntaxTree);

        using (var ms = new MemoryStream())
        {
            var emitResult = compilation.Emit(ms);

            if (!emitResult.Success)
            {
                var failures = emitResult.Diagnostics.Where(diagnostic =>
                  diagnostic.IsWarningAsError ||
                  diagnostic.Severity == DiagnosticSeverity.Error);

                foreach (var diagnostic in failures)
                {
                    Console.Error.WriteLine($"{diagnostic.Id}: {diagnostic.GetMessage()}");
                }

                return null;
            }
            else
            {
                ms.Seek(0, SeekOrigin.Begin);
                var assemblyPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DynamicAssembly.dll");
                File.WriteAllBytes(assemblyPath, ms.ToArray());
                var assembly = Assembly.LoadFrom(assemblyPath);
                return assembly;
            }
        }
    }

    private static IEnumerable<MetadataReference> GetReferences()
    {
        var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic);
        var references = assemblies.Select(x => MetadataReference.CreateFromFile(x.Location));
        return references;
    }
}
