﻿using BenchmarkDotNet.Attributes;
using System.Linq;

namespace ConsoleWindows
{
    public class UserBenchmark
    {
        private int[] _sortedNumbers;

        [Params(10, 100, 1000, 10000, 100000, 1000000)]
        public int Size { get; set; }

        [GlobalSetup]
        public void Setup()
        {
            _sortedNumbers = Enumerable.Range(0, Size).ToArray();
        }

        [Benchmark]
        public bool BinarySearch()
        {
            int target = _sortedNumbers[_sortedNumbers.Length / 2];
            int left = 0;
            int right = _sortedNumbers.Length - 1;

            while (left <= right)
            {
                int middle = (left + right) / 2;

                if (_sortedNumbers[middle] == target)
                    return true;

                if (_sortedNumbers[middle] < target)
                    left = middle + 1;
                else
                    right = middle - 1;
            }

            return false;
        }
    }
}